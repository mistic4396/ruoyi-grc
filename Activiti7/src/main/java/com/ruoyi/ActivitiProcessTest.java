package com.ruoyi;

import cn.hutool.core.io.file.FileReader;
import com.googlecode.aviator.AviatorEvaluator;
import com.googlecode.aviator.Expression;
import org.activiti.bpmn.model.*;
import org.activiti.bpmn.model.Process;
import org.activiti.engine.*;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricActivityInstanceQuery;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.image.ProcessDiagramGenerator;
import org.activiti.image.impl.DefaultProcessDiagramGenerator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;

/**
 * Hello world!
 *
 */
public class ActivitiProcessTest
{
    public static String definitionKey="lossevent_6";
    ProcessEngine processEngine = null;
    RuntimeService runtimeService=null;

    @Before
    public void before() {
        //提前加载这个
        Expression compiledExp = AviatorEvaluator.compile("a>1");
        //创建ProcessEngineConfiguration对象
        ProcessEngineConfiguration configuration = ProcessEngineConfiguration.createProcessEngineConfigurationFromResource("activiti-cfg.xml");
        processEngine = configuration.buildProcessEngine();
        //获取RuntimeService对象
        runtimeService = processEngine.getRuntimeService();


    }

    @After
    public void after() {
        if (processEngine != null) {
            processEngine.close();
        }
    }



    @Test
    public void testDeploy() {

        //获取RepositoryService对象
        RepositoryService repositoryService = processEngine.getRepositoryService();
        //进行部署
        Deployment deployment = repositoryService.createDeployment()
                .addClasspathResource("diagram/lossevent_64.bpmn") //添加bpmn资源
               // .addClasspathResource("diagram/holiday.png")
                .name("事件流程")
                .deploy(); //部署
        //输出部署的一些信息
        System.out.println(ActivitiFormateUtils.formateDeployment(deployment,null));
    }


    @Test
    //查询流程定义
    public void testQuery() {
        //获取RepositoryService对象
        RepositoryService repositoryService = processEngine.getRepositoryService();
        //得到ProcessDefinitionQuery对象
        ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
        //设置条件，并查询出当前的所有流程定义
        List<ProcessDefinition> processDefinitionList = processDefinitionQuery
                .processDefinitionKey("lossevent_7")
                .processDefinitionId("lossevent_7:1:15bd86c3-4775-11eb-a18e-b0359fadfbca")
                .orderByProcessDefinitionVersion()
                .desc()
                .list();
        //输出流程定义信息
        for (ProcessDefinition processDefinition : processDefinitionList) {
            System.out.println( ActivitiFormateUtils.formateProcessDefinition(processDefinition,null));
            GrcPocess grcPocess = ActivitiUtils.pocessALL(repositoryService, processDefinition.getId());
            System.out.println(grcPocess);
        }
    }




    @Test
    /**
     * 删除已经部署的流程定义
     *
     * 注意事项：
     *  ①当我们正在执行的这一套流程没有结束，此时如果要删除流程定义信息就会失败（调用void deleteDeployment(String deploymentId)方法）。
     *  ②当我们正在执行的这一套流程没有结束，此时如果要强制删除流程定义信息，需要调用void deleteDeployment(String deploymentId, boolean cascade)这个方式，将cascade设置为 true。
     */
    public void testDelete() {
        //获取RepositoryService对象
        RepositoryService repositoryService = processEngine.getRepositoryService();
        //删除已经部署的流程定义
        String deploymentId = "e61ebaa3-3dcf-11eb-812e-b0359fadfbca";
        repositoryService.deleteDeployment(deploymentId ,true);
    }

    @Test
    //该流程定义下将不允许启动新的流程实例，同时该流程定义下的所有流程实例将全部挂起暂停执行。
    public void testActivateOrSuspend() {

        //获取RepositoryService对象
        RepositoryService repositoryService = processEngine.getRepositoryService();
        //获取指定key的流程定义
        List<ProcessDefinition> processDefinitionList = repositoryService.createProcessDefinitionQuery().processDefinitionKey(definitionKey).list();
        for (ProcessDefinition processDefinition : processDefinitionList) {
            //如果该流程定义是暂停的
            boolean suspended = processDefinition.isSuspended();
            if (suspended) {
                //如果该流程定义是暂停的，则激活该流程定义下的所有流程实例
                //第一个参数，表示流程定义的id
                //第二个参数，表示是否级联激活该流程定义的流程实例
                //第三个参数，表示激活这个流程定义的时间，如果不填写，从此刻开始
                repositoryService.activateProcessDefinitionById(processDefinition.getId(), true, null);
                System.out.println("流程定义" + processDefinition.getId() + "激活");
            } else {
                //挂起该流程
                repositoryService.suspendProcessDefinitionById(processDefinition.getId(), true, null);
                System.out.println("流程定义" + processDefinition.getId() + "挂起");
            }
        }

    }




    @Test
    public void testProcessFile() throws IOException {
        //获取RepositoryService对象
        RepositoryService repositoryService = processEngine.getRepositoryService();
        //获取ProcessDefinitionQuery对象
        ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
        //设置查询条件,执行查询操作
        List<ProcessDefinition> processDefinitionList = processDefinitionQuery.processDefinitionKey(definitionKey).orderByProcessDefinitionVersion().desc().list();
        //遍历查询结果
        for (ProcessDefinition processDefinition : processDefinitionList) {
            //获取资源名称，即BPMN 图片的名称
            String resourceName = processDefinition.getResourceName();
            //获取资源的输入流，即png图片的输入流
            InputStream resourceNameInputStream = repositoryService.getResourceAsStream(processDefinition.getDeploymentId(), resourceName);
            String resourcePath = "d:" + File.separator + resourceName;
            File file = new File(resourcePath);
            if (!file.exists()) {
                file.getParentFile().mkdirs();
            }
            FileCopyUtils.copy(resourceNameInputStream, new FileOutputStream(resourcePath));


           /* //获取图表资源，即bpmn图片的名称
            String diagramResourceName = processDefinition.getDiagramResourceName();

            InputStream diagramResourceNameInputStream = repositoryService.getResourceAsStream(processDefinition.getDeploymentId(), diagramResourceName);



            String diagramResourcePath = "d:" + File.separator + diagramResourceName;
            File file2 = new File(diagramResourcePath);
            if (!file2.exists()) {
                file2.getParentFile().mkdirs();
            }

            //复制文件

            FileCopyUtils.copy(diagramResourceNameInputStream, new FileOutputStream(diagramResourcePath));*/
        }
    }



}
