package com.ruoyi;

import com.googlecode.aviator.AviatorEvaluator;
import com.googlecode.aviator.Expression;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.bpmn.model.UserTask;
import org.apache.commons.lang3.StringUtils;

import java.util.LinkedList;
import java.util.Map;

public class GrcUserTask {

    private UserTask userTask=null;
    private SequenceFlow upSequenceFlow=null;
    private LinkedList<SequenceFlow> lineList = new LinkedList<SequenceFlow>();


    private String expression="";

    private String assignee="";

    public String getConditionExpression(){
        if(StringUtils.isEmpty(expression)){
            expression =upSequenceFlow.getConditionExpression();
            // 线上为空 默认
            if(StringUtils.isEmpty(expression)){
                expression="true";
            }else{
                expression =expression
                        .replaceAll("\\{" ,"")
                        .replaceAll("\\}" ,"")
                        .replaceAll("\\$" ,"")
                ;
            }


        }
        return expression;
    }






    public GrcUserTask(UserTask userTask, SequenceFlow upSequenceFlow, LinkedList<SequenceFlow> lineList) {
        this.userTask = userTask;
        this.upSequenceFlow = upSequenceFlow;

        this.lineList.addAll(lineList);


    }

    public GrcUserTask(UserTask userTask, SequenceFlow upSequenceFlow) {
        this.userTask = userTask;
        this.upSequenceFlow = upSequenceFlow;
    }

    public GrcUserTask(UserTask userTask) {
        this.userTask = userTask;
    }

    public GrcUserTask() {
    }

    public UserTask getUserTask() {
        return userTask;
    }

    public SequenceFlow getUpSequenceFlow() {
        return upSequenceFlow;
    }

    public LinkedList<SequenceFlow> getLineList() {
        return lineList;
    }

    public void setUserTask(UserTask userTask) {
        this.userTask = userTask;
    }

    public void setUpSequenceFlow(SequenceFlow upSequenceFlow) {
        this.upSequenceFlow = upSequenceFlow;
    }

    public void setLineList(LinkedList<SequenceFlow> lineList) {
        this.lineList = lineList;
    }

    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(String.format(" userTask   id :{%s}   name :{%s}    InFlows :{%s} OutFlows :{%s}  "
                      ,userTask.getId()
                    ,userTask.getName()
                    ,userTask.getIncomingFlows()
                    ,userTask.getOutgoingFlows()

                ));
        stringBuffer.append("\r\n");
        if(upSequenceFlow!=null){
            stringBuffer.append(String.format(" SequenceFlow   id :{%s}   name :{%s}   ref :{%s}    target: {%s}   Condition:  {%s}  "
                    ,upSequenceFlow.getId()
                    ,upSequenceFlow.getName()
                    ,upSequenceFlow.getSourceRef()
                    ,upSequenceFlow.getTargetRef()
                    ,upSequenceFlow.getConditionExpression()

            ));
        }


        stringBuffer.append(String.format(" expression:{%s}   assignee :{%s}   "
                ,expression
                ,assignee


        ));

        stringBuffer.append("\r\n");

        return stringBuffer.toString();
    }

    public String getExpression() {
        return expression;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }
}
