package com.ruoyi;

import com.googlecode.aviator.AviatorEvaluator;
import com.googlecode.aviator.Expression;
import org.activiti.engine.*;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricActivityInstanceQuery;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricVariableInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ActivitiHistoricTest {
    public static String definitionKey="lossevent_6";
    ProcessEngine processEngine = null;
    RuntimeService runtimeService=null;

    @Before
    public void before() {
        //提前加载这个
        Expression compiledExp = AviatorEvaluator.compile("a>1");
        //创建ProcessEngineConfiguration对象
        ProcessEngineConfiguration configuration = ProcessEngineConfiguration.createProcessEngineConfigurationFromResource("activiti-cfg.xml");
        processEngine = configuration.buildProcessEngine();
        //获取RuntimeService对象
        runtimeService = processEngine.getRuntimeService();


    }

    @After
    public void after() {
        if (processEngine != null) {
            processEngine.close();
        }
    }


    @Test
    public void testInstanceQuery() {
        HistoryService historyService = processEngine.getHistoryService();

        List<HistoricProcessInstance> list1 = historyService.createHistoricProcessInstanceQuery()
                .processDefinitionKey(definitionKey)
                .processInstanceBusinessKey("1")
                .orderByProcessInstanceStartTime().desc()
                .list();
        for (HistoricProcessInstance historicProcessInstance : list1) {
            // String processDefinitionId = historicProcessInstance.getProcessDefinitionId();
            String id = historicProcessInstance.getId();
            System.out.println(id);
        }

    }
    @Test
    public void testActivityQuery() {
        HistoryService historyService = processEngine.getHistoryService();
        //1 历史节点
        List<HistoricActivityInstance> finishedlist = historyService.createHistoricActivityInstanceQuery()
                .processInstanceId("dedec098-4efb-11eb-b253-b0359fadfbca")
                //.processDefinitionId("lossevent_6:5:98e6f571-4754-11eb-9285-b0359fadfbca")

               //.finished()
                .orderByHistoricActivityInstanceStartTime().asc()
               // .orderByHistoricActivityInstanceStartTime().asc()

                .list();
        HashMap<String, HashMap<String, Object>> stringStringHashMap = new HashMap<>();
        for (HistoricActivityInstance historicActivityInstance : finishedlist) {
            System.out.println( ActivitiFormateUtils.formateHistoricActivityInstance(historicActivityInstance,null));


            String taskId = historicActivityInstance.getTaskId();
            if(StringUtils.isNotEmpty(taskId)){
                HashMap<String, Object> objectObjectHashMap = new HashMap<>();
                stringStringHashMap.put(taskId,objectObjectHashMap);
                // 2 节点变量
                List<HistoricVariableInstance> list = historyService.createHistoricVariableInstanceQuery().taskId(taskId).list();
                for (HistoricVariableInstance historicVariableInstance : list) {
                    Object value = historicVariableInstance.getValue();
                    String variableName = historicVariableInstance.getVariableName();

                    if("remark".equals(variableName)){
                        //System.out.println(value);
                    }
                    objectObjectHashMap.put(variableName,value);

                }
            }

        }
        stringStringHashMap.forEach((s, stringObjectHashMap) -> {
            System.out.println(s+": "+stringObjectHashMap);
        });


    }





}
