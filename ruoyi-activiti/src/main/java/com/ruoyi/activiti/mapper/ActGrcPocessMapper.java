package com.ruoyi.activiti.mapper;

import java.util.List;
import com.ruoyi.activiti.domain.ActGrcPocess;

/**
 * 流程Mapper接口
 * 
 * @author ruoyi
 * @date 2020-12-05
 */
public interface ActGrcPocessMapper 
{
    /**
     * 查询流程
     * 
     * @param pocessId 流程ID
     * @return 流程
     */
    public ActGrcPocess selectActGrcPocessById(Long pocessId);

    /**
     * 查询流程列表
     * 
     * @param actGrcPocess 流程
     * @return 流程集合
     */
    public List<ActGrcPocess> selectActGrcPocessList(ActGrcPocess actGrcPocess);

    /**
     * 新增流程
     * 
     * @param actGrcPocess 流程
     * @return 结果
     */
    public int insertActGrcPocess(ActGrcPocess actGrcPocess);

    /**
     * 修改流程
     * 
     * @param actGrcPocess 流程
     * @return 结果
     */
    public int updateActGrcPocess(ActGrcPocess actGrcPocess);

    /**
     * 删除流程
     * 
     * @param pocessId 流程ID
     * @return 结果
     */
    public int deleteActGrcPocessById(Long pocessId);

    /**
     * 批量删除流程
     * 
     * @param pocessIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteActGrcPocessByIds(Long[] pocessIds);
}
