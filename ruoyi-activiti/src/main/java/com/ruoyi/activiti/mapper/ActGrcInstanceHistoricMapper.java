package com.ruoyi.activiti.mapper;

import java.util.List;
import com.ruoyi.activiti.domain.ActGrcInstanceHistoric;

/**
 *  流程历史Mapper接口
 * 
 * @author ruoyi
 * @date 2020-12-07
 */
public interface ActGrcInstanceHistoricMapper 
{
    /**
     * 查询 流程历史
     * 
     * @param id  流程历史ID
     * @return  流程历史
     */
    public ActGrcInstanceHistoric selectActGrcInstanceHistoricById(Long id);

    /**
     * 查询 流程历史列表
     * 
     * @param actGrcInstanceHistoric  流程历史
     * @return  流程历史集合
     */
    public List<ActGrcInstanceHistoric> selectActGrcInstanceHistoricList(ActGrcInstanceHistoric actGrcInstanceHistoric);

    /**
     * 新增 流程历史
     * 
     * @param actGrcInstanceHistoric  流程历史
     * @return 结果
     */
    public int insertActGrcInstanceHistoric(ActGrcInstanceHistoric actGrcInstanceHistoric);

    /**
     * 修改 流程历史
     * 
     * @param actGrcInstanceHistoric  流程历史
     * @return 结果
     */
    public int updateActGrcInstanceHistoric(ActGrcInstanceHistoric actGrcInstanceHistoric);

    /**
     * 删除 流程历史
     * 
     * @param id  流程历史ID
     * @return 结果
     */
    public int deleteActGrcInstanceHistoricById(Long id);

    /**
     * 批量删除 流程历史
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteActGrcInstanceHistoricByIds(Long[] ids);
}
