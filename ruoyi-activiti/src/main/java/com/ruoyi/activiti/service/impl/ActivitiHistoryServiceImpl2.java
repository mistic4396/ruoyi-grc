package com.ruoyi.activiti.service.impl;

import com.ruoyi.activiti.domain.dto.ActivitiHighLineDTO;
import com.ruoyi.activiti.service.IActivitiHistoryService;
import org.activiti.bpmn.model.Process;
import org.activiti.bpmn.model.*;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


public class ActivitiHistoryServiceImpl2 implements IActivitiHistoryService {

    @Autowired
    private HistoryService historyService;
    @Autowired
    private RepositoryService repositoryService;


    @Override
    public ActivitiHighLineDTO gethighLine(String instanceId) {




        //查询通过的节点
        HashMap<String, HistoricActivityInstance> finishedMap = getfinishedMap(instanceId);

        //返回通过的节点
        HashMap<String, SequenceFlow> stringSequenceFlowHashMap = new HashMap<>();
        HashMap<String, FlowElement> stringFlowElementHashMap = new HashMap<>();
        //返回未通过的节点
        HashMap<String, SequenceFlow> unstringSequenceFlowHashMap = new HashMap<>();
        HashMap<String, FlowElement> unstringFlowElementHashMap = new HashMap<>();



        //流程图定义
        HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery()
                .processInstanceId(instanceId).singleResult();
        BpmnModel bpmnModel = repositoryService.getBpmnModel(historicProcessInstance.getProcessDefinitionId());
        Process process = bpmnModel.getProcesses().get(0);
        //获取所有的FlowElement信息
        Collection<FlowElement> flowElements = process.getFlowElements();



        for (FlowElement flowElement : flowElements) {
            //判断是否是连线
            if (flowElement instanceof SequenceFlow) {
                SequenceFlow sequenceFlow = (SequenceFlow) flowElement;
                //线的起点和终点 都已办理 ， 线条-->认定通过
                if(finishedMap.containsKey(sequenceFlow.getSourceRef())
                        && finishedMap.containsKey(sequenceFlow.getTargetRef())
                ){
                    stringSequenceFlowHashMap.put(sequenceFlow.getId(),sequenceFlow);
                }else{

                    unstringSequenceFlowHashMap.put(sequenceFlow.getId(),sequenceFlow);
                }
            }

            else{
                //元素简单判断
                if(finishedMap.containsKey(flowElement.getId())
                ){
                    stringFlowElementHashMap.put(flowElement.getId(),flowElement);

                }else{
                    unstringFlowElementHashMap.put(flowElement.getId(),flowElement);
                }

            }
        }

        ActivitiHighLineDTO activitiHighLineDTO =new ActivitiHighLineDTO();
        activitiHighLineDTO.setHighPoint(stringFlowElementHashMap.keySet());
        activitiHighLineDTO.setHighLine(stringSequenceFlowHashMap.keySet());

        activitiHighLineDTO.setWaitingToDo(unstringFlowElementHashMap.keySet());
        activitiHighLineDTO.setiDo(unstringFlowElementHashMap.keySet());

        return activitiHighLineDTO;
    }

    private HashMap<String, HistoricActivityInstance>  getfinishedMap(String instanceId) {
        //获取流程实例 历史节点（已完成）
        HashMap<String, HistoricActivityInstance> finishedMap = new HashMap<>();
        List<HistoricActivityInstance> finishedlist = historyService.createHistoricActivityInstanceQuery()
                .processInstanceId(instanceId)
                .finished()
                .list();
        for (HistoricActivityInstance hActivityInstance : finishedlist) {
           /* System.out.println(String.format("HistoricActivityInstance   Id :{%s}  TaskId :{%s}  Assignee: {%s} ActivityId ：{%s}  ActivityName：{%s} "
                    ,hActivityInstance.getId()
                    ,hActivityInstance.getTaskId()
                    ,hActivityInstance.getAssignee()
                    ,hActivityInstance.getActivityId()
                    ,hActivityInstance.getActivityName()

            ));*/
            finishedMap.put(hActivityInstance.getActivityId(),hActivityInstance);
        }
        return finishedMap;
    }
}
