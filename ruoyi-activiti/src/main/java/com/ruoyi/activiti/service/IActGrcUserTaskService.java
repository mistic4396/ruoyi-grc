package com.ruoyi.activiti.service;

import java.util.List;
import com.ruoyi.activiti.domain.ActGrcUserTask;

/**
 * 工作流用户节点Service接口
 * 
 * @author ruoyi
 * @date 2020-12-06
 */
public interface IActGrcUserTaskService 
{
    /**
     * 查询工作流用户节点
     * 
     * @param id 工作流用户节点ID
     * @return 工作流用户节点
     */
    public ActGrcUserTask selectActGrcUserTaskById(Long id);

    /**
     * 查询工作流用户节点列表
     * 
     * @param actGrcUserTask 工作流用户节点
     * @return 工作流用户节点集合
     */
    public List<ActGrcUserTask> selectActGrcUserTaskList(ActGrcUserTask actGrcUserTask);

    /**
     * 新增工作流用户节点
     * 
     * @param actGrcUserTask 工作流用户节点
     * @return 结果
     */
    public int insertActGrcUserTask(ActGrcUserTask actGrcUserTask);

    /**
     * 修改工作流用户节点
     * 
     * @param actGrcUserTask 工作流用户节点
     * @return 结果
     */
    public int updateActGrcUserTask(ActGrcUserTask actGrcUserTask);

    /**
     * 批量删除工作流用户节点
     * 
     * @param ids 需要删除的工作流用户节点ID
     * @return 结果
     */
    public int deleteActGrcUserTaskByIds(Long[] ids);

    /**
     * 删除工作流用户节点信息
     * 
     * @param id 工作流用户节点ID
     * @return 结果
     */
    public int deleteActGrcUserTaskById(Long id);
}
