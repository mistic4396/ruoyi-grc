package com.ruoyi.activiti.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.activiti.mapper.ActGrcPocessMapper;
import com.ruoyi.activiti.domain.ActGrcPocess;
import com.ruoyi.activiti.service.IActGrcPocessService;

/**
 * 流程Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-12-05
 */
@Service
public class ActGrcPocessServiceImpl implements IActGrcPocessService 
{
    @Autowired
    private ActGrcPocessMapper actGrcPocessMapper;

    /**
     * 查询流程
     * 
     * @param pocessId 流程ID
     * @return 流程
     */
    @Override
    public ActGrcPocess selectActGrcPocessById(Long pocessId)
    {
        return actGrcPocessMapper.selectActGrcPocessById(pocessId);
    }

    /**
     * 查询流程列表
     * 
     * @param actGrcPocess 流程
     * @return 流程
     */
    @Override
    public List<ActGrcPocess> selectActGrcPocessList(ActGrcPocess actGrcPocess)
    {
        return actGrcPocessMapper.selectActGrcPocessList(actGrcPocess);
    }

    /**
     * 新增流程
     * 
     * @param actGrcPocess 流程
     * @return 结果
     */
    @Override
    public int insertActGrcPocess(ActGrcPocess actGrcPocess)
    {
        actGrcPocess.setCreateTime(DateUtils.getNowDate());
        return actGrcPocessMapper.insertActGrcPocess(actGrcPocess);
    }

    /**
     * 修改流程
     * 
     * @param actGrcPocess 流程
     * @return 结果
     */
    @Override
    public int updateActGrcPocess(ActGrcPocess actGrcPocess)
    {
        actGrcPocess.setUpdateTime(DateUtils.getNowDate());
        return actGrcPocessMapper.updateActGrcPocess(actGrcPocess);
    }

    /**
     * 批量删除流程
     * 
     * @param pocessIds 需要删除的流程ID
     * @return 结果
     */
    @Override
    public int deleteActGrcPocessByIds(Long[] pocessIds)
    {
        return actGrcPocessMapper.deleteActGrcPocessByIds(pocessIds);
    }

    /**
     * 删除流程信息
     * 
     * @param pocessId 流程ID
     * @return 结果
     */
    @Override
    public int deleteActGrcPocessById(Long pocessId)
    {
        return actGrcPocessMapper.deleteActGrcPocessById(pocessId);
    }
}
