package com.ruoyi.activiti.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.activiti.domain.ActGrcPocess;
import com.ruoyi.activiti.service.IActGrcPocessService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 流程Controller
 * 
 * @author ruoyi
 * @date 2020-12-05
 */
@RestController
@RequestMapping("/activiti/pocess")
public class ActGrcPocessController extends BaseController
{
    @Autowired
    private IActGrcPocessService actGrcPocessService;

    /**
     * 查询流程列表
     */
    @PreAuthorize("@ss.hasPermi('activiti:pocess:list')")
    @GetMapping("/list")
    public TableDataInfo list(ActGrcPocess actGrcPocess)
    {
        startPage();
        List<ActGrcPocess> list = actGrcPocessService.selectActGrcPocessList(actGrcPocess);
        return getDataTable(list);
    }

    /**
     * 导出流程列表
     */
    @PreAuthorize("@ss.hasPermi('activiti:pocess:export')")
    @Log(title = "流程", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ActGrcPocess actGrcPocess)
    {
        List<ActGrcPocess> list = actGrcPocessService.selectActGrcPocessList(actGrcPocess);
        ExcelUtil<ActGrcPocess> util = new ExcelUtil<ActGrcPocess>(ActGrcPocess.class);
        return util.exportExcel(list, "pocess");
    }

    /**
     * 获取流程详细信息
     */
    @PreAuthorize("@ss.hasPermi('activiti:pocess:query')")
    @GetMapping(value = "/{pocessId}")
    public AjaxResult getInfo(@PathVariable("pocessId") Long pocessId)
    {
        return AjaxResult.success(actGrcPocessService.selectActGrcPocessById(pocessId));
    }

    /**
     * 新增流程
     */
    @PreAuthorize("@ss.hasPermi('activiti:pocess:add')")
    @Log(title = "流程", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ActGrcPocess actGrcPocess)
    {
        return toAjax(actGrcPocessService.insertActGrcPocess(actGrcPocess));
    }

    /**
     * 修改流程
     */
    @PreAuthorize("@ss.hasPermi('activiti:pocess:edit')")
    @Log(title = "流程", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ActGrcPocess actGrcPocess)
    {
        return toAjax(actGrcPocessService.updateActGrcPocess(actGrcPocess));
    }

    /**
     * 删除流程
     */
    @PreAuthorize("@ss.hasPermi('activiti:pocess:remove')")
    @Log(title = "流程", businessType = BusinessType.DELETE)
	@DeleteMapping("/{pocessIds}")
    public AjaxResult remove(@PathVariable Long[] pocessIds)
    {
        return toAjax(actGrcPocessService.deleteActGrcPocessByIds(pocessIds));
    }
}
