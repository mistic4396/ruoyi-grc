package com.ruoyi.activiti.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.activiti.domain.ActGrcPocessVariables;
import com.ruoyi.activiti.service.IActGrcPocessVariablesService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 流程参数Controller
 * 
 * @author ruoyi
 * @date 2020-12-06
 */
@RestController
@RequestMapping("/activiti/variables")
public class ActGrcPocessVariablesController extends BaseController
{
    @Autowired
    private IActGrcPocessVariablesService actGrcPocessVariablesService;

    /**
     * 查询流程参数列表
     */
    @PreAuthorize("@ss.hasPermi('activiti:variables:list')")
    @GetMapping("/list")
    public TableDataInfo list(ActGrcPocessVariables actGrcPocessVariables)
    {
        startPage();
        List<ActGrcPocessVariables> list = actGrcPocessVariablesService.selectActGrcPocessVariablesList(actGrcPocessVariables);
        return getDataTable(list);
    }

    /**
     * 导出流程参数列表
     */
    @PreAuthorize("@ss.hasPermi('activiti:variables:export')")
    @Log(title = "流程参数", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ActGrcPocessVariables actGrcPocessVariables)
    {
        List<ActGrcPocessVariables> list = actGrcPocessVariablesService.selectActGrcPocessVariablesList(actGrcPocessVariables);
        ExcelUtil<ActGrcPocessVariables> util = new ExcelUtil<ActGrcPocessVariables>(ActGrcPocessVariables.class);
        return util.exportExcel(list, "variables");
    }

    /**
     * 获取流程参数详细信息
     */
    @PreAuthorize("@ss.hasPermi('activiti:variables:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(actGrcPocessVariablesService.selectActGrcPocessVariablesById(id));
    }

    /**
     * 新增流程参数
     */
    @PreAuthorize("@ss.hasPermi('activiti:variables:add')")
    @Log(title = "流程参数", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ActGrcPocessVariables actGrcPocessVariables)
    {
        return toAjax(actGrcPocessVariablesService.insertActGrcPocessVariables(actGrcPocessVariables));
    }

    /**
     * 修改流程参数
     */
    @PreAuthorize("@ss.hasPermi('activiti:variables:edit')")
    @Log(title = "流程参数", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ActGrcPocessVariables actGrcPocessVariables)
    {
        return toAjax(actGrcPocessVariablesService.updateActGrcPocessVariables(actGrcPocessVariables));
    }

    /**
     * 删除流程参数
     */
    @PreAuthorize("@ss.hasPermi('activiti:variables:remove')")
    @Log(title = "流程参数", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(actGrcPocessVariablesService.deleteActGrcPocessVariablesByIds(ids));
    }
}
