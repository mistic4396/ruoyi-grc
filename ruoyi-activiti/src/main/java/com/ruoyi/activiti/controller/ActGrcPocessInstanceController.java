package com.ruoyi.activiti.controller;

import com.github.pagehelper.Page;
import com.ruoyi.activiti.domain.*;
import com.ruoyi.activiti.service.IActGrcPocessInstanceService;
import com.ruoyi.activiti.utils.GrcUserTask;
import com.ruoyi.activiti.utils.graph.SeriesGraph;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.page.TableSupport;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.activiti.engine.history.HistoricProcessInstance;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 流程实例Controller
 * 
 * @author ruoyi
 * @date 2020-12-06
 */
@Api("流程管理")
@RestController
@RequestMapping("/activiti/instance")
public class ActGrcPocessInstanceController extends BaseController
{
    @Autowired
    private IActGrcPocessInstanceService actGrcPocessInstanceService;




    @ApiOperation("参数校验")
    @PreAuthorize("@ss.hasPermi('activiti:instance:list')")
    @PostMapping("/parmiterTest")

    public AjaxResult parmiter(@Validated @RequestBody ActWorkflowFormDataParmiter actGrcPocessVariables)
    {
        System.out.println("成功进入");
        return toAjax(1);
    }

    @PreAuthorize("@ss.hasPermi('activiti:instance:list')")
    @PostMapping("/vueParmiter")

    public AjaxResult testVue( @RequestBody  ActGrcPocessVariables actGrcPocessVariables)
    {
        System.out.println("成功进入");
        return AjaxResult.success(actGrcPocessVariables);
    }


    /**
     * 查询流程实例列表
     */
    @ApiOperation("获取代办列表")
    @PreAuthorize("@ss.hasPermi('activiti:instance:list')")
    @GetMapping("/list")
    public TableDataInfo list(ActGrcPocessVariables actGrcPocessVariables)
    {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Page<ActGrcPocessVariables> taskList = actGrcPocessInstanceService.getTaskList(actGrcPocessVariables, pageDomain);
        return getDataTable(taskList);
    }

    @ApiOperation("启动流程前，根据流程定义获取下一个节点")
    @PreAuthorize("@ss.hasPermi('activiti:instance:list')")
    @GetMapping("/startNext/{befinitionKey}")
    public AjaxResult startNextGrcUserTask(@PathVariable("befinitionKey") String befinitionKey)
    {
        List<GrcUserTask> grcUserTasks = actGrcPocessInstanceService.startGetNextGrcUserTask(befinitionKey);
        List<ActGrcPocessVariables> actGrcPocessVariables = actGrcPocessInstanceService.buildActGrcPocessVariables(grcUserTasks);
        return AjaxResult.success(actGrcPocessVariables);
    }

    @ApiOperation("启动")
    @PreAuthorize("@ss.hasPermi('activiti:instance:list')")
    @PostMapping("/start")
    @Log(title = "流程", businessType = BusinessType.FLOW_START)
    public AjaxResult start(@Validated @RequestBody ActGrcPocessVariablesStart actGrcPocessVariables )
    {
        ActGrcPocessVariables target = new ActGrcPocessVariables();
        BeanUtils.copyProperties(actGrcPocessVariables,target);
        ActGrcPocessInstance actGrcPocessInstance = actGrcPocessInstanceService.startAndCompleteProcessInstance(target);
        return AjaxResult.success(actGrcPocessInstance.getProInstanceId());
    }

    @ApiOperation("获取流程执行详情")
    @PreAuthorize("@ss.hasPermi('activiti:instance:list')")
    @GetMapping("/getProcessInstance/{instanceId}")
    public AjaxResult getProcessInstance(@PathVariable("instanceId") String instanceId)
    {
        ActGrcPocessVariables processInstance = actGrcPocessInstanceService.getProcessInstance(instanceId);
        return AjaxResult.success(processInstance);
    }




    @ApiOperation("跳转")
    @PreAuthorize("@ss.hasPermi('activiti:instance:list')")
    @PostMapping("/skip")
    @Log(title = "流程", businessType = BusinessType.FLOW_SKIP)
    public AjaxResult skip(@Validated @RequestBody ActGrcPocessVariablesSkip actGrcPocessVariables)
    {
        ActGrcPocessVariables target = new ActGrcPocessVariables();
        BeanUtils.copyProperties(actGrcPocessVariables,target);
        actGrcPocessInstanceService.skipProcessInstance(target);
        return toAjax(1);
    }

    @ApiOperation("获取下一步")
    @PreAuthorize("@ss.hasPermi('activiti:instance:list')")
    @GetMapping("/getNextUserTaskList/{taskId}")

    public AjaxResult getNextUserTaskList(@PathVariable("taskId") String taskId)
    {
        List<GrcUserTask> grcUserTasks = actGrcPocessInstanceService.NextUserTask(taskId);
        List<ActGrcPocessVariables> collect = actGrcPocessInstanceService.buildActGrcPocessVariables(grcUserTasks);



        return AjaxResult.success(collect);
    }

    @ApiOperation("提交")
    @PreAuthorize("@ss.hasPermi('activiti:instance:list')")
    @PostMapping("/complete")
    @Log(title = "流程", businessType = BusinessType.FLOW_COMPLETE)
    public AjaxResult complete(@Validated @RequestBody ActGrcPocessVariablesComplete actGrcPocessVariables)
    {
        ActGrcPocessVariables target = new ActGrcPocessVariables();
        BeanUtils.copyProperties(actGrcPocessVariables,target);
        actGrcPocessInstanceService.completeProcessInstance(target);
        return toAjax(1);
    }

    @ApiOperation("获取最新的历史节点")
    @PreAuthorize("@ss.hasPermi('activiti:instance:list')")
    @GetMapping("/getPreUserTaskList/{taskId}")
    @Log(title = "流程", businessType = BusinessType.FLOW_SKIP)
    public TableDataInfo getPreUserTaskList(@PathVariable("taskId") String taskId)
    {
        List<GrcUserTask> grcUserTasks = actGrcPocessInstanceService.getPreGrcUserTask(taskId);
        List<ActGrcPocessVariables> collect = actGrcPocessInstanceService.buildActGrcPocessVariables(grcUserTasks);
        return getDataTable(collect);
    }

    @ApiOperation("获取历史信息")
    @PreAuthorize("@ss.hasPermi('activiti:instance:list')")
    @GetMapping("/getProcessHistoric/{processInstanceId}")
    public AjaxResult getProcessHistoric(@PathVariable("processInstanceId") String processInstanceId)
    {
        SeriesGraph seriesGraphBYproInstanceId = actGrcPocessInstanceService.getSeriesGraphBYproInstanceId(processInstanceId);
        return  AjaxResult.success(seriesGraphBYproInstanceId);
    }

    @ApiOperation("获取历史信息")
    @PreAuthorize("@ss.hasPermi('activiti:instance:list')")
    @GetMapping("/getProcessHistoricBYbusinessId/{businessId}/{befinitionKey}")
    public AjaxResult getProcessHistoricBYbusinessId(@PathVariable("businessId") String businessId,
            @PathVariable("befinitionKey") String befinitionKey
        )
    {
        List<HistoricProcessInstance> historicProcessInstance = actGrcPocessInstanceService.getHistoricProcessInstance(businessId, befinitionKey);
        if(historicProcessInstance.size()>0){
            SeriesGraph seriesGraphBYproInstanceId = actGrcPocessInstanceService.getSeriesGraphBYproInstanceId(historicProcessInstance.get(0).getId());
            return  AjaxResult.success(seriesGraphBYproInstanceId);
        }
        // SeriesGraph seriesGraphBYproInstanceId = actGrcPocessInstanceService.getSeriesGraphBYproInstanceId(processInstanceId);
        return  AjaxResult.error("没有进行的流程");
    }


//--------------------------------------------------------------------------------------------------------

    /**
     * 导出流程实例列表
     */
    @PreAuthorize("@ss.hasPermi('activiti:instance:export')")
    @Log(title = "流程实例", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ActGrcPocessInstance actGrcPocessInstance)
    {
        List<ActGrcPocessInstance> list = actGrcPocessInstanceService.selectActGrcPocessInstanceList(actGrcPocessInstance);
        ExcelUtil<ActGrcPocessInstance> util = new ExcelUtil<ActGrcPocessInstance>(ActGrcPocessInstance.class);
        return util.exportExcel(list, "instance");
    }

    /**
     * 获取流程实例详细信息
     */
    @PreAuthorize("@ss.hasPermi('activiti:instance:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(actGrcPocessInstanceService.selectActGrcPocessInstanceById(id));
    }

    /**
     * 新增流程实例
     */
    @PreAuthorize("@ss.hasPermi('activiti:instance:add')")
    @Log(title = "流程实例", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ActGrcPocessInstance actGrcPocessInstance)
    {
        return toAjax(actGrcPocessInstanceService.insertActGrcPocessInstance(actGrcPocessInstance));
    }

    /**
     * 修改流程实例
     */
    @PreAuthorize("@ss.hasPermi('activiti:instance:edit')")
    @Log(title = "流程实例", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ActGrcPocessInstance actGrcPocessInstance)
    {
        return toAjax(actGrcPocessInstanceService.updateActGrcPocessInstance(actGrcPocessInstance));
    }

    /**
     * 删除流程实例
     */
    @PreAuthorize("@ss.hasPermi('activiti:instance:remove')")
    @Log(title = "流程实例", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(actGrcPocessInstanceService.deleteActGrcPocessInstanceByIds(ids));
    }
}
