package com.ruoyi.activiti.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.activiti.domain.ActGrcUserTask;
import com.ruoyi.activiti.service.IActGrcUserTaskService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 工作流用户节点Controller
 * 
 * @author ruoyi
 * @date 2020-12-06
 */
@RestController
@RequestMapping("/activiti/task")
public class ActGrcUserTaskController extends BaseController
{
    @Autowired
    private IActGrcUserTaskService actGrcUserTaskService;

    /**
     * 查询工作流用户节点列表
     */
    @PreAuthorize("@ss.hasPermi('activiti:task:list')")
    @GetMapping("/list")
    public TableDataInfo list(ActGrcUserTask actGrcUserTask)
    {
        startPage();
        List<ActGrcUserTask> list = actGrcUserTaskService.selectActGrcUserTaskList(actGrcUserTask);
        return getDataTable(list);
    }

    /**
     * 导出工作流用户节点列表
     */
    @PreAuthorize("@ss.hasPermi('activiti:task:export')")
    @Log(title = "工作流用户节点", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ActGrcUserTask actGrcUserTask)
    {
        List<ActGrcUserTask> list = actGrcUserTaskService.selectActGrcUserTaskList(actGrcUserTask);
        ExcelUtil<ActGrcUserTask> util = new ExcelUtil<ActGrcUserTask>(ActGrcUserTask.class);
        return util.exportExcel(list, "task");
    }

    /**
     * 获取工作流用户节点详细信息
     */
    @PreAuthorize("@ss.hasPermi('activiti:task:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(actGrcUserTaskService.selectActGrcUserTaskById(id));
    }

    /**
     * 新增工作流用户节点
     */
    @PreAuthorize("@ss.hasPermi('activiti:task:add')")
    @Log(title = "工作流用户节点", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ActGrcUserTask actGrcUserTask)
    {
        return toAjax(actGrcUserTaskService.insertActGrcUserTask(actGrcUserTask));
    }

    /**
     * 修改工作流用户节点
     */
    @PreAuthorize("@ss.hasPermi('activiti:task:edit')")
    @Log(title = "工作流用户节点", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ActGrcUserTask actGrcUserTask)
    {
        return toAjax(actGrcUserTaskService.updateActGrcUserTask(actGrcUserTask));
    }

    /**
     * 删除工作流用户节点
     */
    @PreAuthorize("@ss.hasPermi('activiti:task:remove')")
    @Log(title = "工作流用户节点", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(actGrcUserTaskService.deleteActGrcUserTaskByIds(ids));
    }
}
