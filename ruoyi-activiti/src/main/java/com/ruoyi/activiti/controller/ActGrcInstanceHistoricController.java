package com.ruoyi.activiti.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.activiti.domain.ActGrcInstanceHistoric;
import com.ruoyi.activiti.service.IActGrcInstanceHistoricService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 *  流程历史Controller
 * 
 * @author ruoyi
 * @date 2020-12-07
 */
@RestController
@RequestMapping("/activiti/historic")
public class ActGrcInstanceHistoricController extends BaseController
{
    @Autowired
    private IActGrcInstanceHistoricService actGrcInstanceHistoricService;

    /**
     * 查询 流程历史列表
     */
    @PreAuthorize("@ss.hasPermi('activiti:historic:list')")
    @GetMapping("/list")
    public TableDataInfo list(ActGrcInstanceHistoric actGrcInstanceHistoric)
    {
        startPage();
        List<ActGrcInstanceHistoric> list = actGrcInstanceHistoricService.selectActGrcInstanceHistoricList(actGrcInstanceHistoric);
        return getDataTable(list);
    }

    /**
     * 导出 流程历史列表
     */
    @PreAuthorize("@ss.hasPermi('activiti:historic:export')")
    @Log(title = " 流程历史", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ActGrcInstanceHistoric actGrcInstanceHistoric)
    {
        List<ActGrcInstanceHistoric> list = actGrcInstanceHistoricService.selectActGrcInstanceHistoricList(actGrcInstanceHistoric);
        ExcelUtil<ActGrcInstanceHistoric> util = new ExcelUtil<ActGrcInstanceHistoric>(ActGrcInstanceHistoric.class);
        return util.exportExcel(list, "historic");
    }

    /**
     * 获取 流程历史详细信息
     */
    @PreAuthorize("@ss.hasPermi('activiti:historic:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(actGrcInstanceHistoricService.selectActGrcInstanceHistoricById(id));
    }

    /**
     * 新增 流程历史
     */
    @PreAuthorize("@ss.hasPermi('activiti:historic:add')")
    @Log(title = " 流程历史", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ActGrcInstanceHistoric actGrcInstanceHistoric)
    {
        return toAjax(actGrcInstanceHistoricService.insertActGrcInstanceHistoric(actGrcInstanceHistoric));
    }

    /**
     * 修改 流程历史
     */
    @PreAuthorize("@ss.hasPermi('activiti:historic:edit')")
    @Log(title = " 流程历史", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ActGrcInstanceHistoric actGrcInstanceHistoric)
    {
        return toAjax(actGrcInstanceHistoricService.updateActGrcInstanceHistoric(actGrcInstanceHistoric));
    }

    /**
     * 删除 流程历史
     */
    @PreAuthorize("@ss.hasPermi('activiti:historic:remove')")
    @Log(title = " 流程历史", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(actGrcInstanceHistoricService.deleteActGrcInstanceHistoricByIds(ids));
    }
}
