package com.ruoyi.activiti.utils.listener.impl;

import com.ruoyi.activiti.domain.ActGrcPocess;
import com.ruoyi.activiti.service.IActGrcPocessService;
import com.ruoyi.activiti.utils.listener.BaseElementListener;
import com.ruoyi.common.utils.spring.SpringUtils;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.engine.delegate.DelegateExecution;


public class TestListener extends BaseElementListener {

    @Override
    public void startInstance(String processInstanceBusinessKey) {
        IActGrcPocessService bean = SpringUtils.getBean(IActGrcPocessService.class);
        ActGrcPocess actGrcPocess = bean.selectActGrcPocessById(Long.valueOf(processInstanceBusinessKey));
        System.out.println(actGrcPocess);

        actGrcPocess.setStatus("2");

        int i = bean.updateActGrcPocess(actGrcPocess);


    }

    @Override
    public void endInstance(String processInstanceBusinessKey) {

    }

    @Override
    public void backInstance(String processInstanceBusinessKey, SequenceFlow sequenceFlow) {

    }

    @Override
    public void flowInstance(String processInstanceBusinessKey, SequenceFlow sequenceFlow) {

    }

    @Override
    public void doExecut(DelegateExecution execution) {

    }
}
