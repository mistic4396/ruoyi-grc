package com.ruoyi.activiti.utils.listener;

import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.engine.delegate.*;
import org.activiti.engine.impl.persistence.entity.ExecutionEntityImpl;
import org.activiti.engine.impl.persistence.entity.TaskEntityImpl;
import org.apache.commons.lang3.StringUtils;

public abstract class BaseElementListener  implements ExecutionListener, TaskListener {
    @Override
    //ExecutionListener
    public void notify(DelegateExecution execution) {
        ExecutionEntityImpl executionEntity=  (ExecutionEntityImpl)execution;
        String activityId = executionEntity.getActivityId();
        String activityName = executionEntity.getActivityName();
        String eventName = executionEntity.getEventName();
        String processInstanceBusinessKey = executionEntity.getProcessInstanceBusinessKey();
        String processInstanceId = executionEntity.getProcessInstanceId();
        FlowElement currentFlowElement = executionEntity.getCurrentFlowElement();

        if(StringUtils.isEmpty(activityId)){
            //整个流程的操作
            if(BaseExecutionListener.EVENTNAME_START.equals(eventName)){
                System.out.println("流程启动");
                startInstance(processInstanceBusinessKey);
            }else if(BaseExecutionListener.EVENTNAME_END.equals(eventName)){
                System.out.println("流程结束");
                endInstance(processInstanceBusinessKey);
            }

        }
        else if(currentFlowElement instanceof SequenceFlow){
            SequenceFlow  sequenceFlow =(SequenceFlow) currentFlowElement;
            if("${acti_back}".equals(sequenceFlow.getConditionExpression())){
                System.out.println("流程退回：" + sequenceFlow.getTargetRef() );
                backInstance(processInstanceBusinessKey,sequenceFlow);
            }else {
                flowInstance(processInstanceBusinessKey,sequenceFlow);
            }

        }

        doExecut(execution);
        /*System.out.println("------ExecutionListener-------"+String.format(
                "  activityId : %s   activityName : %s   eventName : %s    processInstanceBusinessKey : %s  processInstanceId : %s"
                ,activityId
                ,activityName
                , eventName
                ,processInstanceBusinessKey
                ,processInstanceId
        ));*/
    }


    public abstract void startInstance ( String processInstanceBusinessKey);
    public abstract void endInstance ( String processInstanceBusinessKey);
    public abstract void backInstance ( String processInstanceBusinessKey , SequenceFlow  sequenceFlow);
    public abstract void flowInstance ( String processInstanceBusinessKey , SequenceFlow  sequenceFlow);

    public abstract void doExecut (DelegateExecution execution);




    @Override
    //TaskListener
    public void notify(DelegateTask delegateTask) {
        TaskEntityImpl taskEntity=(TaskEntityImpl)delegateTask;

        String name = taskEntity.getName();
        String eventName = taskEntity.getEventName();

        String assignee = taskEntity.getAssignee();
        String processInstanceBusinessKey = taskEntity.getProcessInstance().getProcessInstanceBusinessKey();
       /* System.out.println("------TaskListener-------"+String.format(

                "  name : %s  eventName : %s  assignee : %s  processInstanceBusinessKey : %s  "
                ,name
                , eventName
                , assignee
                ,processInstanceBusinessKey
        ));*/
    }



}
