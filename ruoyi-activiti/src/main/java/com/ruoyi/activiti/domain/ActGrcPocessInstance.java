package com.ruoyi.activiti.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 流程实例对象 act_grc_pocess_instance
 * 
 * @author ruoyi
 * @date 2020-12-06
 */
public class ActGrcPocessInstance extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 流程id */
    @Excel(name = "流程id")
    private String proInstanceId;

    /** 业务id */
    @Excel(name = "业务id")
    private String businessId;

    /** 业务名称 */
    @Excel(name = "业务名称")
    private String businessName;

    //任务id
    private String taskId;
    //执行id
    private String executionId;
    //目标跳转的id
    private String targetId;

    public String getTargetId() {

        return targetId;
    }

    public void setExecutionId(String executionId) {
        this.executionId = executionId;
    }

    public String getExecutionId() {
        return executionId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProInstanceId(String proInstanceId) 
    {
        this.proInstanceId = proInstanceId;
    }

    public String getProInstanceId() 
    {
        return proInstanceId;
    }
    public void setBusinessId(String businessId) 
    {
        this.businessId = businessId;
    }

    public String getBusinessId() 
    {
        return businessId;
    }
    public void setBusinessName(String businessName) 
    {
        this.businessName = businessName;
    }

    public String getBusinessName() 
    {
        return businessName;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("proInstanceId", getProInstanceId())
            .append("businessId", getBusinessId())
            .append("businessName", getBusinessName())
            .toString();
    }
}
