package com.ruoyi.activiti.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.Map;

/**
 * 流程启动参数对象 act_grc_pocess_variables
 * 
 * @author ruoyi
 * @date 2020-12-06
 */
public class ActGrcPocessVariablesSkip
{
    @NotBlank(message = "下一个节点的受理人不能为空")
    private String user;
    @NotBlank(message = "跳转的目标id不能为空")
    private String targetId;

    @NotBlank(message = "任务id不能为空")
    private String taskId;


    /** 参数 */
    private Map<String,Object> vars;

    public String getUser() {
        return user;
    }

    public String getTargetId() {
        return targetId;
    }

    public String getTaskId() {
        return taskId;
    }

    public Map<String, Object> getVars() {
        return vars;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public void setVars(Map<String, Object> vars) {
        this.vars = vars;
    }
}
