package com.ruoyi.activiti.domain;

import com.ruoyi.activiti.utils.GrcUserTask;
import com.ruoyi.common.core.domain.entity.SysUser;
import org.activiti.bpmn.model.EndEvent;
import org.activiti.bpmn.model.UserTask;
import org.activiti.engine.history.HistoricActivityInstance;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import javax.validation.constraints.NotBlank;
import java.util.*;

/**
 * 流程启动参数对象 act_grc_pocess_variables
 * 
 * @author ruoyi
 * @date 2020-12-06
 */
public class ActGrcPocessVariables extends BaseEntity
{


    /** $column.columnComment */
    private Long id;

    /** 流程实例id */
    @Excel(name = "流程id")
    private String proInstanceId;



    /** 流程实例业务id */
    @Excel(name = "业务id")
    private String businessId;

    /** 流程实例业务名称 */
    @Excel(name = "业务名称")
    private String businessName;

    //任务id
    private String taskId;
    //执行id
    private String executionId;


    //选择提交的用户
    private List<SysUser> userList;



    /** 流程定义的key */
    private String befinitionKey;
    private String processDefinitionId;
    /** 节点受理人 */
    private String user;
    /** 参数 */
    private Map<String,Object> vars;
    //目标跳转的id
    private String targetId;



    private String activityId;
    private String activityName;
    private String expression;



    //是否是结束节点
    private Boolean willEnd=false;

    public void setUserList(List<SysUser> userList) {
        this.userList = userList;
    }

    public List<SysUser> getUserList() {
        return userList;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public ActGrcPocessVariables(HistoricActivityInstance historicActivityInstance) {
        this.activityId=historicActivityInstance.getActivityId();

        this.activityName=historicActivityInstance.getActivityName();


        Date startTime = historicActivityInstance.getStartTime();

        Date endTime = historicActivityInstance.getEndTime();


        this.taskId=historicActivityInstance.getTaskId();
        this.user=historicActivityInstance.getAssignee();


        this.setCreateTime(historicActivityInstance.getStartTime());
        this.setUpdateTime(historicActivityInstance.getEndTime());

    }

    public ActGrcPocessVariables(GrcUserTask grcuserTask) {
        if(grcuserTask.getUserTask()!=null){
            UserTask userTask = grcuserTask.getUserTask();
            this.user=userTask.getAssignee();
            this.expression = grcuserTask.getConditionExpression();
            this.activityId=userTask.getId();
            if("acti_back".equals(this.expression)){
                this.activityName="退回-"+userTask.getName();
            }else{
                this.activityName=userTask.getName();
            }


            ArrayList<SysUser> sysUsers = new ArrayList<SysUser>();
            sysUsers.add(getSysUser("admin","超级管理员"));
            sysUsers.add(getSysUser("admin2","小张"));
            this.userList = sysUsers;
        }  else if(grcuserTask.getEndEvent() !=null){
            EndEvent endEvent = grcuserTask.getEndEvent();
            this.expression = grcuserTask.getConditionExpression();
            this.activityId=endEvent.getId();
            this.activityName=endEvent.getName();

            this.willEnd=true;
        }


    }

    private SysUser getSysUser(String userName,String nickName) {
        SysUser sysUser = new SysUser();
        sysUser.setUserName(userName);
        sysUser.setNickName(nickName);
        return sysUser;
    }

    public ActGrcPocessVariables() {

    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public void setWillEnd(Boolean willEnd) {
        this.willEnd = willEnd;
    }

    public Boolean getWillEnd() {
        return willEnd;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityId() {
        return activityId;
    }

    public String getActivityName() {
        return activityName;
    }

    public String getExecutionId() {
        return executionId;
    }

    public void setExecutionId(String executionId) {
        this.executionId = executionId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setBefinitionKey(String befinitionKey) {
        this.befinitionKey = befinitionKey;
    }

    public String getBefinitionKey() {
        return befinitionKey;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProInstanceId(String proInstanceId) 
    {
        this.proInstanceId = proInstanceId;
    }

    public String getProInstanceId() 
    {
        return proInstanceId;
    }
    public void setBusinessId(String businessId) 
    {
        this.businessId = businessId;
    }

    public String getBusinessId() 
    {
        return businessId;
    }
    public void setBusinessName(String businessName) 
    {
        this.businessName = businessName;
    }

    public String getBusinessName() 
    {
        return businessName;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {

        this.user = user;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("proInstanceId", getProInstanceId())
            .append("businessId", getBusinessId())
            .append("businessName", getBusinessName())
            .toString();
    }

    public void setVars(Map<String, Object> vars) {
        this.vars = vars;
    }

    public Map<String, Object> getVars() {
        return vars;
    }
}
