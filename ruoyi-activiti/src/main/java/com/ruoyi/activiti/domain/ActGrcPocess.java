package com.ruoyi.activiti.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 流程对象 act_grc_pocess
 * 
 * @author ruoyi
 * @date 2020-12-05
 */
public class ActGrcPocess extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long pocessId;

    /** 名称 */
    @Excel(name = "名称")
    private String pocessName;

    /** 邮件模板 */
    @Excel(name = "邮件模板")
    private String mailTemplate;

    /** 工作流id */
    private String definitionId;

    /** 工作流key */
    private String definitionKey;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setPocessId(Long pocessId) 
    {
        this.pocessId = pocessId;
    }

    public Long getPocessId() 
    {
        return pocessId;
    }
    public void setPocessName(String pocessName) 
    {
        this.pocessName = pocessName;
    }

    public String getPocessName() 
    {
        return pocessName;
    }
    public void setMailTemplate(String mailTemplate) 
    {
        this.mailTemplate = mailTemplate;
    }

    public String getMailTemplate() 
    {
        return mailTemplate;
    }
    public void setDefinitionId(String definitionId) 
    {
        this.definitionId = definitionId;
    }

    public String getDefinitionId() 
    {
        return definitionId;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("pocessId", getPocessId())
            .append("pocessName", getPocessName())
            .append("mailTemplate", getMailTemplate())
            .append("definitionId", getDefinitionId())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }

    public String getDefinitionKey() {
        return definitionKey;
    }

    public void setDefinitionKey(String definitionKey) {
        this.definitionKey = definitionKey;
    }
}
