package com.ruoyi.grc.cach.impl;

import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.grc.cach.ICachService;
import com.ruoyi.system.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userCachService")
public class UserCachService implements ICachService<SysUser> {

    @Autowired
    private SysUserMapper userMapper;

    @Override
    public List<SysUser> getCachDBList() {

        return userMapper.allUserList();

    }
}
