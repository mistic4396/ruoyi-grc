package com.ruoyi.grc.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 损失事件对象 t21_ldc_lossevent
 * 
 * @author liuqi
 * @date 2021-01-17
 */
public class T21LdcLossevent extends BaseEntity
{
    private static final long serialVersionUID = 1L;


    private Long[] areaIds;
    private Long[] deptIds;



    /** ID */
    private Long id;

    /** 编号 */
    @Excel(name = "编号")
    private String code;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 总金额 */
    @Excel(name = "总金额")
    private BigDecimal totalAmount;

    /** 类别 */
    @Excel(name = "类别")
    private String type;

    /** 评分 */
    @Excel(name = "评分")
    private Long rateType;

    /** 颜色 */
    @Excel(name = "颜色")
    private String colorType;

    /** 发生时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发生时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date findTime;

    /** 发生机构 */
    @Excel(name = "发生机构")
    private Long findDeptId;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 部门ID */
    @Excel(name = "部门ID")
    private Long deptId;

    public Long[] getDeptIds() {
        return deptIds;
    }

    public void setDeptIds(Long[] deptIds) {
        this.deptIds = deptIds;
    }

    public Long[] getAreaIds() {
        return areaIds;
    }

    public void setAreaIds(Long[] areaIds) {
        this.areaIds = areaIds;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setTotalAmount(BigDecimal totalAmount) 
    {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalAmount() 
    {
        return totalAmount;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }

    public Long getRateType() {
        return rateType;
    }

    public void setRateType(Long rateType) {
        this.rateType = rateType;
    }

    public void setColorType(String colorType)
    {
        this.colorType = colorType;
    }

    public String getColorType() 
    {
        return colorType;
    }
    public void setFindTime(Date findTime) 
    {
        this.findTime = findTime;
    }

    public Date getFindTime() 
    {
        return findTime;
    }

    public Long getFindDeptId() {
        return findDeptId;
    }

    public void setFindDeptId(Long findDeptId) {
        this.findDeptId = findDeptId;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("name", getName())
            .append("totalAmount", getTotalAmount())
            .append("type", getType())
            .append("rateType", getRateType())
            .append("colorType", getColorType())
            .append("findTime", getFindTime())
            .append("findDeptId", getFindDeptId())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("deptId", getDeptId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
