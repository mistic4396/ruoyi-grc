package com.ruoyi.grc.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 损失明细对象 t21_ldc_item
 * 
 * @author liuqi
 * @date 2021-01-14
 */
public class T21LdcItem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 事件id */
    @Excel(name = "事件id")
    private Long losseventId;

    /** 编号 */
    @Excel(name = "编号")
    private String code;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 部门ID */
    @Excel(name = "部门ID")
    private Long deptId;

    /** 明细金额 */
    @Excel(name = "明细金额")
    private BigDecimal itemAmount;

    /** 类别 */
    @Excel(name = "类别")
    private String type;

    /** 发生时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发生时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date findTime;

    /** 发生机构 */
    @Excel(name = "发生机构")
    private Long findDeptId;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setLosseventId(Long losseventId) 
    {
        this.losseventId = losseventId;
    }

    public Long getLosseventId() 
    {
        return losseventId;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setItemAmount(BigDecimal itemAmount) 
    {
        this.itemAmount = itemAmount;
    }

    public BigDecimal getItemAmount() 
    {
        return itemAmount;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setFindTime(Date findTime) 
    {
        this.findTime = findTime;
    }

    public Date getFindTime() 
    {
        return findTime;
    }
    public void setFindDeptId(Long findDeptId) 
    {
        this.findDeptId = findDeptId;
    }

    public Long getFindDeptId() 
    {
        return findDeptId;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("losseventId", getLosseventId())
            .append("code", getCode())
            .append("name", getName())
            .append("deptId", getDeptId())
            .append("itemAmount", getItemAmount())
            .append("type", getType())
            .append("findTime", getFindTime())
            .append("findDeptId", getFindDeptId())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
