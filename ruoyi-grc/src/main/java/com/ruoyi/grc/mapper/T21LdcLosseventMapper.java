package com.ruoyi.grc.mapper;

import java.util.List;
import com.ruoyi.grc.domain.T21LdcLossevent;
import com.ruoyi.system.domain.SysUserRole;

/**
 * 损失事件Mapper接口
 * 
 * @author liuqi
 * @date 2021-01-17
 */
public interface T21LdcLosseventMapper 
{
    /**
     * 查询损失事件
     * 
     * @param id 损失事件ID
     * @return 损失事件
     */
    public T21LdcLossevent selectT21LdcLosseventById(Long id);

    /**
     * 查询损失事件列表
     * 
     * @param t21LdcLossevent 损失事件
     * @return 损失事件集合
     */
    public List<T21LdcLossevent> selectT21LdcLosseventList(T21LdcLossevent t21LdcLossevent);

    /**
     * 新增损失事件
     * 
     * @param t21LdcLossevent 损失事件
     * @return 结果
     */
    public int insertT21LdcLossevent(T21LdcLossevent t21LdcLossevent);

    /**
     * 修改损失事件
     * 
     * @param t21LdcLossevent 损失事件
     * @return 结果
     */
    public int updateT21LdcLossevent(T21LdcLossevent t21LdcLossevent);

    /**
     * 删除损失事件
     * 
     * @param id 损失事件ID
     * @return 结果
     */
    public int deleteT21LdcLosseventById(Long id);

    /**
     * 批量删除损失事件
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteT21LdcLosseventByIds(Long[] ids);



}
