package com.ruoyi.grc.mapper;

import com.ruoyi.grc.domain.LosseventAreaRelation;
import com.ruoyi.grc.domain.T21LdcLossevent;
import com.ruoyi.system.domain.SysUserRole;

import java.util.List;

/**
 * 损失事件Mapper接口
 * 
 * @author liuqi
 * @date 2021-01-17
 */
public interface LosseventAreaRelationMapper
{
    /**
     * 批量新增
     * @param list
     * @return
     */
    public int batchLosseventArea(List<LosseventAreaRelation> list);

    /**
     * 查询关联
     * @param losseventId
     * @return
     */
    public List<Long> selectAreaIdListByLosseventId(Long losseventId);

    /**
     * 删除
     * @param losseventId
     * @return
     */
    public int deleteAreaIdByLosseventId(Long losseventId);


}
