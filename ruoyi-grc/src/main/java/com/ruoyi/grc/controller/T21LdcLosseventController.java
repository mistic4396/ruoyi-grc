package com.ruoyi.grc.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.grc.domain.T21LdcLossevent;
import com.ruoyi.grc.service.IT21LdcLosseventService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 损失事件Controller
 * 
 * @author liuqi
 * @date 2021-01-17
 */
@RestController
@RequestMapping("/grc/lossevent")
public class T21LdcLosseventController extends BaseController
{
    @Autowired
    private IT21LdcLosseventService t21LdcLosseventService;

    /**
     * 查询损失事件列表
     */
    @PreAuthorize("@ss.hasPermi('grc:lossevent:list')")
    @GetMapping("/list")
    public TableDataInfo list(T21LdcLossevent t21LdcLossevent)
    {
        startPage();
        List<T21LdcLossevent> list = t21LdcLosseventService.selectT21LdcLosseventList(t21LdcLossevent);
        return getDataTable(list);
    }

    /**
     * 导出损失事件列表
     */
    @PreAuthorize("@ss.hasPermi('grc:lossevent:export')")
    @Log(title = "损失事件", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(T21LdcLossevent t21LdcLossevent)
    {
        List<T21LdcLossevent> list = t21LdcLosseventService.selectT21LdcLosseventList(t21LdcLossevent);
        ExcelUtil<T21LdcLossevent> util = new ExcelUtil<T21LdcLossevent>(T21LdcLossevent.class);
        return util.exportExcel(list, "lossevent");
    }

    /**
     * 获取损失事件详细信息
     */
    @PreAuthorize("@ss.hasPermi('grc:lossevent:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(t21LdcLosseventService.selectT21LdcLosseventById(id));
    }

    /**
     * 新增损失事件
     */
    @PreAuthorize("@ss.hasPermi('grc:lossevent:add')")
    @Log(title = "损失事件", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody T21LdcLossevent t21LdcLossevent)
    {
        return toAjax(t21LdcLosseventService.insertT21LdcLossevent(t21LdcLossevent));
    }

    /**
     * 修改损失事件
     */
    @PreAuthorize("@ss.hasPermi('grc:lossevent:edit')")
    @Log(title = "损失事件", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody T21LdcLossevent t21LdcLossevent)
    {
        return toAjax(t21LdcLosseventService.updateT21LdcLossevent(t21LdcLossevent));
    }

    /**
     * 删除损失事件
     */
    @PreAuthorize("@ss.hasPermi('grc:lossevent:remove')")
    @Log(title = "损失事件", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(t21LdcLosseventService.deleteT21LdcLosseventByIds(ids));
    }
}
