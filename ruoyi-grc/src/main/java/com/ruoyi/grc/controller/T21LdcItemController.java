package com.ruoyi.grc.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.grc.domain.T21LdcItem;
import com.ruoyi.grc.service.IT21LdcItemService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 损失明细Controller
 * 
 * @author liuqi
 * @date 2021-01-14
 */
@RestController
@RequestMapping("/grc/item")
public class T21LdcItemController extends BaseController
{
    @Autowired
    private IT21LdcItemService t21LdcItemService;

    /**
     * 查询损失明细列表
     */
    @PreAuthorize("@ss.hasPermi('grc:item:list')")
    @GetMapping("/list")
    public TableDataInfo list(T21LdcItem t21LdcItem)
    {
        startPage();
        List<T21LdcItem> list = t21LdcItemService.selectT21LdcItemList(t21LdcItem);
        return getDataTable(list);
    }

    /**
     * 导出损失明细列表
     */
    @PreAuthorize("@ss.hasPermi('grc:item:export')")
    @Log(title = "损失明细", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(T21LdcItem t21LdcItem)
    {
        List<T21LdcItem> list = t21LdcItemService.selectT21LdcItemList(t21LdcItem);
        ExcelUtil<T21LdcItem> util = new ExcelUtil<T21LdcItem>(T21LdcItem.class);
        return util.exportExcel(list, "item");
    }

    /**
     * 获取损失明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('grc:item:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(t21LdcItemService.selectT21LdcItemById(id));
    }

    /**
     * 新增损失明细
     */
    @PreAuthorize("@ss.hasPermi('grc:item:add')")
    @Log(title = "损失明细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody T21LdcItem t21LdcItem)
    {

        int i = t21LdcItemService.insertT21LdcItem(t21LdcItem);

        return toAjax(i);
    }

    /**
     * 修改损失明细
     */
    @PreAuthorize("@ss.hasPermi('grc:item:edit')")
    @Log(title = "损失明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody T21LdcItem t21LdcItem)
    {
        return toAjax(t21LdcItemService.updateT21LdcItem(t21LdcItem));
    }

    /**
     * 删除损失明细
     */
    @PreAuthorize("@ss.hasPermi('grc:item:remove')")
    @Log(title = "损失明细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(t21LdcItemService.deleteT21LdcItemByIds(ids));
    }
}
