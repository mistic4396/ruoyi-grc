package com.ruoyi.grc.controller;

import java.util.List;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.grc.domain.T00FileInfo;
import com.ruoyi.grc.service.IT00FileInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 附件信息Controller
 * 
 * @author liuqi
 * @date 2021-01-24
 */
@RestController
@RequestMapping("/grc/fileInfo")
public class T00FileInfoController extends BaseController
{
    @Autowired
    private IT00FileInfoService t00FileInfoService;

    /**
     * 查询附件信息列表
     */
    @PreAuthorize("@ss.hasPermi('grc:fileInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(T00FileInfo t00FileInfo)
    {
        startPage();
        List<T00FileInfo> list = t00FileInfoService.selectT00FileInfoList(t00FileInfo);
        return getDataTable(list);
    }

    /**
     * 导出附件信息列表
     */
    @PreAuthorize("@ss.hasPermi('grc:fileInfo:export')")
    @Log(title = "附件信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(T00FileInfo t00FileInfo)
    {
        List<T00FileInfo> list = t00FileInfoService.selectT00FileInfoList(t00FileInfo);
        ExcelUtil<T00FileInfo> util = new ExcelUtil<T00FileInfo>(T00FileInfo.class);
        return util.exportExcel(list, "fileInfo");
    }

    /**
     * 获取附件信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('grc:fileInfo:query')")
    @GetMapping(value = "/download/{fileId}")
    public void getDownloadInfo(@PathVariable("fileId") Long fileId , HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        T00FileInfo t00FileInfo = t00FileInfoService.selectT00FileInfoById(fileId);
        String fileName = t00FileInfo.getFileName();
        String filePath = t00FileInfo.getFilePath();

        // 本地资源路径
        String localPath = RuoYiConfig.getProfile();
        // 数据库资源地址
        String downloadPath = localPath + StringUtils.substringAfter(filePath, Constants.RESOURCE_PREFIX);
        // 下载名称
        String downloadName = StringUtils.substringAfterLast(downloadPath, "/");
        response.setCharacterEncoding("utf-8");
        response.setContentType("multipart/form-data");
        response.setHeader("Content-Disposition",
                "attachment;fileName=" + FileUtils.setFileDownloadHeader(request, fileName));
        FileUtils.writeBytes(downloadPath, response.getOutputStream());
        // return AjaxResult.success();
    }

    @PreAuthorize("@ss.hasPermi('grc:fileInfo:query')")
    @GetMapping(value = "/{fileId}")
    public AjaxResult getInfo(@PathVariable("fileId") Long fileId)
    {
        return AjaxResult.success(t00FileInfoService.selectT00FileInfoById(fileId));
    }

    /**
     * 新增附件信息
     */
    @PreAuthorize("@ss.hasPermi('grc:fileInfo:add')")
    @Log(title = "附件信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody T00FileInfo t00FileInfo)
    {
        return toAjax(t00FileInfoService.insertT00FileInfo(t00FileInfo));
    }

    /**
     * 修改附件信息
     */
    @PreAuthorize("@ss.hasPermi('grc:fileInfo:edit')")
    @Log(title = "附件信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody T00FileInfo t00FileInfo)
    {
        return toAjax(t00FileInfoService.updateT00FileInfo(t00FileInfo));
    }

    /**
     * 删除附件信息
     */
    @PreAuthorize("@ss.hasPermi('grc:fileInfo:remove')")
    @Log(title = "附件信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{fileIds}")
    public AjaxResult remove(@PathVariable Long[] fileIds)
    {
        return toAjax(t00FileInfoService.deleteT00FileInfoByIds(fileIds));
    }
}
