package com.ruoyi.grc.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.grc.domain.T01Seq;
import com.ruoyi.grc.service.IT01SeqService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 编号生成Controller
 * 
 * @author liuqi
 * @date 2021-01-15
 */
@RestController
@RequestMapping("/grc/seq")
public class T01SeqController extends BaseController
{
    @Autowired
    private IT01SeqService t01SeqService;

    /**
     * 查询编号生成列表
     */
    @PreAuthorize("@ss.hasPermi('grc:seq:list')")
    @GetMapping("/list")
    public TableDataInfo list(T01Seq t01Seq)
    {
        startPage();
        List<T01Seq> list = t01SeqService.selectT01SeqList(t01Seq);
        return getDataTable(list);
    }

    /**
     * 导出编号生成列表
     */
    @PreAuthorize("@ss.hasPermi('grc:seq:export')")
    @Log(title = "编号生成", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(T01Seq t01Seq)
    {

        List<T01Seq> list = t01SeqService.selectT01SeqList(t01Seq);
        ExcelUtil<T01Seq> util = new ExcelUtil<T01Seq>(T01Seq.class);
        return util.exportExcel(list, "seq");
    }

    /**
     * 获取编号生成详细信息
     */
    @PreAuthorize("@ss.hasPermi('grc:seq:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(t01SeqService.selectT01SeqById(id));
    }

    /**
     * 新增编号生成
     */
    @PreAuthorize("@ss.hasPermi('grc:seq:add')")
    @Log(title = "编号生成", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody T01Seq t01Seq)
    {
        return toAjax(t01SeqService.insertT01Seq(t01Seq));
    }

    /**
     * 修改编号生成
     */
    @PreAuthorize("@ss.hasPermi('grc:seq:edit')")
    @Log(title = "编号生成", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody T01Seq t01Seq)
    {
        return toAjax(t01SeqService.updateT01Seq(t01Seq));
    }

    /**
     * 删除编号生成
     */
    @PreAuthorize("@ss.hasPermi('grc:seq:remove')")
    @Log(title = "编号生成", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(t01SeqService.deleteT01SeqByIds(ids));
    }
}
