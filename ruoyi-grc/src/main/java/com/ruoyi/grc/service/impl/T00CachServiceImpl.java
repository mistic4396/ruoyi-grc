package com.ruoyi.grc.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.grc.mapper.T00CachMapper;
import com.ruoyi.grc.domain.T00Cach;
import com.ruoyi.grc.service.IT00CachService;

/**
 * 缓存sqlService业务层处理
 * 
 * @author liuqi
 * @date 2021-01-19
 */
@Service
public class T00CachServiceImpl implements IT00CachService 
{
    @Autowired
    private T00CachMapper t00CachMapper;

    /**
     * 查询缓存sql
     * 
     * @param id 缓存sqlID
     * @return 缓存sql
     */
    @Override
    public T00Cach selectT00CachById(Long id)
    {
        return t00CachMapper.selectT00CachById(id);
    }

    /**
     * 查询缓存sql列表
     * 
     * @param t00Cach 缓存sql
     * @return 缓存sql
     */
    @Override
    public List<T00Cach> selectT00CachList(T00Cach t00Cach)
    {
        return t00CachMapper.selectT00CachList(t00Cach);
    }

    /**
     * 新增缓存sql
     * 
     * @param t00Cach 缓存sql
     * @return 结果
     */
    @Override
    public int insertT00Cach(T00Cach t00Cach)
    {
        return t00CachMapper.insertT00Cach(t00Cach);
    }

    /**
     * 修改缓存sql
     * 
     * @param t00Cach 缓存sql
     * @return 结果
     */
    @Override
    public int updateT00Cach(T00Cach t00Cach)
    {
        return t00CachMapper.updateT00Cach(t00Cach);
    }

    /**
     * 批量删除缓存sql
     * 
     * @param ids 需要删除的缓存sqlID
     * @return 结果
     */
    @Override
    public int deleteT00CachByIds(Long[] ids)
    {
        return t00CachMapper.deleteT00CachByIds(ids);
    }

    /**
     * 删除缓存sql信息
     * 
     * @param id 缓存sqlID
     * @return 结果
     */
    @Override
    public int deleteT00CachById(Long id)
    {
        return t00CachMapper.deleteT00CachById(id);
    }
}
