package com.ruoyi.grc.activiti.listener.impl;

import com.ruoyi.activiti.utils.listener.BaseElementListener;
import com.ruoyi.activiti.utils.listener.BaseUserListener;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.grc.cach.ICachService;
import com.ruoyi.grc.cach.impl.DepCachService;
import com.ruoyi.grc.domain.T21LdcLossevent;
import com.ruoyi.grc.service.IT21LdcLosseventService;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.impl.persistence.entity.ExecutionEntityImpl;

import java.util.HashMap;
import java.util.List;


public class LosseventUserListener extends BaseUserListener {


    private void beasUpdate(String businessKey, String status) {
        IT21LdcLosseventService beanService = SpringUtils.getBean(IT21LdcLosseventService.class);




        T21LdcLossevent bean=new T21LdcLossevent();
        bean.setId(Long.parseLong(businessKey));
        bean.setStatus(status);
        beanService.updateT21LdcLossevent(bean);
    }
    @Override
    public void doUserTask(ExecutionEntityImpl executionEntity, HashMap<String, String> extensionPropertieMap, String businessKey) {
        String acti_status = extensionPropertieMap.get("acti_status");
        if(StringUtils.isNotEmpty(acti_status)){
            beasUpdate(businessKey,acti_status);
        }

    }
}
