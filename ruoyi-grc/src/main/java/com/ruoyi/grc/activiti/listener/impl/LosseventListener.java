package com.ruoyi.grc.activiti.listener.impl;

import com.ruoyi.activiti.domain.ActGrcPocess;
import com.ruoyi.activiti.service.IActGrcPocessService;
import com.ruoyi.activiti.utils.listener.BaseElementListener;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.grc.domain.T21LdcLossevent;
import com.ruoyi.grc.service.IT21LdcLosseventService;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.engine.delegate.DelegateExecution;


public class LosseventListener extends BaseElementListener {


    private void beasUpdate(String processInstanceBusinessKey, String status) {
        IT21LdcLosseventService beanService = SpringUtils.getBean(IT21LdcLosseventService.class);
        T21LdcLossevent bean=new T21LdcLossevent();
        bean.setId(Long.parseLong(processInstanceBusinessKey));
        bean.setStatus(status);
        beanService.updateT21LdcLossevent(bean);
    }



    @Override
    public void backInstance(String processInstanceBusinessKey, SequenceFlow sequenceFlow) {
        beasUpdate(processInstanceBusinessKey, "1");
    }

    @Override
    public void startInstance(String processInstanceBusinessKey) {
        beasUpdate(processInstanceBusinessKey, "2");
    }


    @Override
    public void endInstance(String processInstanceBusinessKey) {
        beasUpdate(processInstanceBusinessKey, "3");
    }





    @Override
    public void flowInstance(String processInstanceBusinessKey, SequenceFlow sequenceFlow) {

    }

    @Override
    public void doExecut(DelegateExecution execution) {

    }
}
