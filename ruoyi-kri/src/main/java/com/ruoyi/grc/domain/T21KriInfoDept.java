package com.ruoyi.grc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * KRI指标关联部门对象 t21_kri_info_dept
 * 
 * @author liuqi
 * @date 2021-01-22
 */
public class T21KriInfoDept extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主表id */
    @Excel(name = "主表id")
    private Long majorId;

    /** 机构id */
    @Excel(name = "机构id")
    private Long deptId;

    public void setMajorId(Long majorId) 
    {
        this.majorId = majorId;
    }

    public Long getMajorId() 
    {
        return majorId;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("majorId", getMajorId())
            .append("deptId", getDeptId())
            .toString();
    }
}
