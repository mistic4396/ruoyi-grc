package com.ruoyi.grc.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.grc.domain.T21KriInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.grc.mapper.T21KriInfoItemMapper;
import com.ruoyi.grc.domain.T21KriInfoItem;
import com.ruoyi.grc.service.IT21KriInfoItemService;

/**
 * KRI指标明细Service业务层处理
 * 
 * @author liuqi
 * @date 2021-01-23
 */
@Service
public class T21KriInfoItemServiceImpl implements IT21KriInfoItemService 
{
    @Autowired
    private T21KriInfoItemMapper t21KriInfoItemMapper;

    /**
     * 查询KRI指标明细
     * 
     * @param id KRI指标明细ID
     * @return KRI指标明细
     */
    @Override
    public T21KriInfoItem selectT21KriInfoItemById(Long id)
    {

        T21KriInfoItem t21KriInfoItem=   t21KriInfoItemMapper.selectT21KriInfoItemById(id);
        doSelectRelationes(t21KriInfoItem);



        return t21KriInfoItem ;
    }

    /**
     * 查询KRI指标明细列表
     * 
     * @param t21KriInfoItem KRI指标明细
     * @return KRI指标明细
     */
    @Override
    public List<T21KriInfoItem> selectT21KriInfoItemList(T21KriInfoItem t21KriInfoItem)
    {
        return t21KriInfoItemMapper.selectT21KriInfoItemList(t21KriInfoItem);
    }

    /**
     * 新增KRI指标明细
     * 
     * @param t21KriInfoItem KRI指标明细
     * @return 结果
     */
    @Override
    public int insertT21KriInfoItem(T21KriInfoItem t21KriInfoItem)
    {
        t21KriInfoItem.setCreateTime(DateUtils.getNowDate());
        //编号初始
        //t21KriInfoItem.setCode(SeqUtils.getNextCode("T000xx"));

        doInsertRelationes(t21KriInfoItem);
        return t21KriInfoItemMapper.insertT21KriInfoItem(t21KriInfoItem);
    }

    /**
     * 修改KRI指标明细
     * 
     * @param t21KriInfoItem KRI指标明细
     * @return 结果
     */
    @Override
    public int updateT21KriInfoItem(T21KriInfoItem t21KriInfoItem)
    {
        t21KriInfoItem.setUpdateTime(DateUtils.getNowDate());
        doUpdateRelation(t21KriInfoItem);
        return t21KriInfoItemMapper.updateT21KriInfoItem(t21KriInfoItem);
    }

    /**
     * 批量删除KRI指标明细
     * 
     * @param ids 需要删除的KRI指标明细ID
     * @return 结果
     */
    @Override
    public int deleteT21KriInfoItemByIds(Long[] ids)
    {
        return t21KriInfoItemMapper.deleteT21KriInfoItemByIds(ids);
    }

    /**
     * 删除KRI指标明细信息
     * 
     * @param id KRI指标明细ID
     * @return 结果
     */
    @Override
    public int deleteT21KriInfoItemById(Long id)
    {
        doDeleteRelationes(id);
        return t21KriInfoItemMapper.deleteT21KriInfoItemById(id);
    }


    /**
    * 查询关系表表
    * @param t21KriInfoItem
    */
    private void doSelectRelationes(T21KriInfoItem t21KriInfoItem) {
        
        Long  id = t21KriInfoItem.getId();


    }

    /**
    * 更新关系表表
    * @param t21KriInfoItem
    */
    private void doUpdateRelation(T21KriInfoItem t21KriInfoItem){

                Long  id = t21KriInfoItem.getId();
        // 删除
        doDeleteRelationes(id);
        //插入
        doInsertRelationes(t21KriInfoItem);
    }

    /**
     * 删除关系表
     * @param id
     */
    private void doDeleteRelationes(Long id) {
        // t21KriInfoItemRelationMapper.deleteAreaIdById(id);
    }

    /**
     * 插入关系表
     * @param t21KriInfoItem
     */
    private void doInsertRelationes(T21KriInfoItem t21KriInfoItem) {
        //doT21KriInfoItemRelationInsert(t21KriInfoItem.getId(),t21KriInfoItem.getXxxxIds());

    }

}
