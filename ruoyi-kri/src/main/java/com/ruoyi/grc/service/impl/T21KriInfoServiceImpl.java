package com.ruoyi.grc.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import cn.hutool.core.util.ArrayUtil;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.grc.domain.T21KriInfoDept;
import com.ruoyi.grc.mapper.T21KriInfoDeptMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.grc.mapper.T21KriInfoMapper;
import com.ruoyi.grc.domain.T21KriInfo;
import com.ruoyi.grc.service.IT21KriInfoService;

/**
 * KRI指标信息Service业务层处理
 * 
 * @author liuqi
 * @date 2021-01-22
 */
@Service
public class T21KriInfoServiceImpl implements IT21KriInfoService 
{
    @Autowired
    private T21KriInfoMapper t21KriInfoMapper;

    /**
     * 查询KRI指标信息
     * 
     * @param id KRI指标信息ID
     * @return KRI指标信息
     */
    @Override
    public T21KriInfo selectT21KriInfoById(Long id)
    {

        T21KriInfo t21KriInfo = t21KriInfoMapper.selectT21KriInfoById(id);
        doSelectRelationes(t21KriInfo);
        return t21KriInfo;
    }



    /**
     * 查询KRI指标信息列表
     * 
     * @param t21KriInfo KRI指标信息
     * @return KRI指标信息
     */
    @Override
    public List<T21KriInfo> selectT21KriInfoList(T21KriInfo t21KriInfo)
    {
        return t21KriInfoMapper.selectT21KriInfoList(t21KriInfo);
    }

    /**
     * 新增KRI指标信息
     * 
     * @param t21KriInfo KRI指标信息
     * @return 结果
     */
    @Override
    public int insertT21KriInfo(T21KriInfo t21KriInfo)
    {
        t21KriInfo.setCreateTime(DateUtils.getNowDate());
        //编号初始
        //t21KriInfo.setCode(SeqUtils.getNextCode("T000xx"));

        doInsertRelationes(t21KriInfo);
        return t21KriInfoMapper.insertT21KriInfo(t21KriInfo);
    }

    /**
     * 修改KRI指标信息
     * 
     * @param t21KriInfo KRI指标信息
     * @return 结果
     */
    @Override
    public int updateT21KriInfo(T21KriInfo t21KriInfo)
    {
        t21KriInfo.setUpdateTime(DateUtils.getNowDate());
        doUpdateRelation(t21KriInfo);
        return t21KriInfoMapper.updateT21KriInfo(t21KriInfo);
    }

    /**
     * 批量删除KRI指标信息
     * 
     * @param ids 需要删除的KRI指标信息ID
     * @return 结果
     */
    @Override
    public int deleteT21KriInfoByIds(Long[] ids)
    {
        return t21KriInfoMapper.deleteT21KriInfoByIds(ids);
    }

    /**
     * 删除KRI指标信息信息
     * 
     * @param id KRI指标信息ID
     * @return 结果
     */
    @Override
    public int deleteT21KriInfoById(Long id)
    {
        doDeleteRelationes(id);
        return t21KriInfoMapper.deleteT21KriInfoById(id);
    }


    /**
     * 查询关系表表
     * @param t21KriInfo
     */
    private void doSelectRelationes(T21KriInfo t21KriInfo) {
        Long id = t21KriInfo.getId();
        Long[] t21KriInfoDeptIds = ArrayUtil.toArray(t21KriInfoDeptMapper.selectT21KriInfoDeptBymajorId(id).stream()
                .map(t21KriInfoDept -> t21KriInfoDept.getDeptId()).collect(Collectors.toList()), Long.class);

        t21KriInfo.setDeptIds(t21KriInfoDeptIds);
    }

    /**
    * 更新关系表表
    * @param t21KriInfo
    */
    private void doUpdateRelation(T21KriInfo t21KriInfo){

                // 删除
        doDeleteRelationes(t21KriInfo.getId());
        //插入
        doInsertRelationes(t21KriInfo);
    }

    /**
     * 删除关系表
     * @param id
     */
    private void doDeleteRelationes(Long id) {
        t21KriInfoDeptMapper.deleteAreaIdBymajorId(id);

    }

    /**
     * 插入关系表
     * @param t21KriInfo
     */
    private void doInsertRelationes(T21KriInfo t21KriInfo) {
        //doT21KriInfoRelationInsert(t21KriInfo.getId(),t21KriInfo.getXxxxIds());
        doT21KriInfoDeptInsert(t21KriInfo.getId(),t21KriInfo.getDeptIds());

    }

    @Autowired
    private T21KriInfoDeptMapper t21KriInfoDeptMapper;

    private void doT21KriInfoDeptInsert(Long majorId, Long[] relationIds) {
        if (StringUtils.isNotNull(relationIds))
        {
            List<T21KriInfoDept> list = new ArrayList<T21KriInfoDept>();
            for (Long relationId : relationIds)
            {
                T21KriInfoDept relation = new T21KriInfoDept();
                relation.setMajorId(majorId);
                relation.setDeptId(relationId);
                list.add(relation);
            }
            if (list.size() > 0)
            {
                t21KriInfoDeptMapper.batchT21KriInfoDept(list);
            }
        }
    }

}
