package com.ruoyi.grc.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.grc.domain.T21KriInfo;
import com.ruoyi.grc.service.IT21KriInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * KRI指标信息Controller
 * 
 * @author liuqi
 * @date 2021-01-22
 */
@RestController
@RequestMapping("/kri/Info")
public class T21KriInfoController extends BaseController
{
    @Autowired
    private IT21KriInfoService t21KriInfoService;

    /**
     * 查询KRI指标信息列表
     */
    @PreAuthorize("@ss.hasPermi('kri:Info:list')")
    @GetMapping("/list")
    public TableDataInfo list(T21KriInfo t21KriInfo)
    {
        startPage();
        List<T21KriInfo> list = t21KriInfoService.selectT21KriInfoList(t21KriInfo);
        return getDataTable(list);
    }

    /**
     * 导出KRI指标信息列表
     */
    @PreAuthorize("@ss.hasPermi('kri:Info:export')")
    @Log(title = "KRI指标信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(T21KriInfo t21KriInfo)
    {
        List<T21KriInfo> list = t21KriInfoService.selectT21KriInfoList(t21KriInfo);
        ExcelUtil<T21KriInfo> util = new ExcelUtil<T21KriInfo>(T21KriInfo.class);
        return util.exportExcel(list, "Info");
    }

    /**
     * 获取KRI指标信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('kri:Info:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(t21KriInfoService.selectT21KriInfoById(id));
    }

    /**
     * 新增KRI指标信息
     */
    @PreAuthorize("@ss.hasPermi('kri:Info:add')")
    @Log(title = "KRI指标信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody T21KriInfo t21KriInfo)
    {
        return toAjax(t21KriInfoService.insertT21KriInfo(t21KriInfo));
    }

    /**
     * 修改KRI指标信息
     */
    @PreAuthorize("@ss.hasPermi('kri:Info:edit')")
    @Log(title = "KRI指标信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody T21KriInfo t21KriInfo)
    {
        return toAjax(t21KriInfoService.updateT21KriInfo(t21KriInfo));
    }

    /**
     * 删除KRI指标信息
     */
    @PreAuthorize("@ss.hasPermi('kri:Info:remove')")
    @Log(title = "KRI指标信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(t21KriInfoService.deleteT21KriInfoByIds(ids));
    }
}
