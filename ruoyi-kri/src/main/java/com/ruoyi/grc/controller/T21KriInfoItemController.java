package com.ruoyi.grc.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.grc.domain.T21KriInfoItem;
import com.ruoyi.grc.service.IT21KriInfoItemService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * KRI指标明细Controller
 * 
 * @author liuqi
 * @date 2021-01-23
 */
@RestController
@RequestMapping("/kri/item")
public class T21KriInfoItemController extends BaseController
{
    @Autowired
    private IT21KriInfoItemService t21KriInfoItemService;

    /**
     * 查询KRI指标明细列表
     */
    @PreAuthorize("@ss.hasPermi('kri:item:list')")
    @GetMapping("/list")
    public TableDataInfo list(T21KriInfoItem t21KriInfoItem)
    {
        startPage();
        List<T21KriInfoItem> list = t21KriInfoItemService.selectT21KriInfoItemList(t21KriInfoItem);
        return getDataTable(list);
    }

    /**
     * 导出KRI指标明细列表
     */
    @PreAuthorize("@ss.hasPermi('kri:item:export')")
    @Log(title = "KRI指标明细", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(T21KriInfoItem t21KriInfoItem)
    {
        List<T21KriInfoItem> list = t21KriInfoItemService.selectT21KriInfoItemList(t21KriInfoItem);
        ExcelUtil<T21KriInfoItem> util = new ExcelUtil<T21KriInfoItem>(T21KriInfoItem.class);
        return util.exportExcel(list, "item");
    }

    /**
     * 获取KRI指标明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('kri:item:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(t21KriInfoItemService.selectT21KriInfoItemById(id));
    }

    /**
     * 新增KRI指标明细
     */
    @PreAuthorize("@ss.hasPermi('kri:item:add')")
    @Log(title = "KRI指标明细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody T21KriInfoItem t21KriInfoItem)
    {
        return toAjax(t21KriInfoItemService.insertT21KriInfoItem(t21KriInfoItem));
    }

    /**
     * 修改KRI指标明细
     */
    @PreAuthorize("@ss.hasPermi('kri:item:edit')")
    @Log(title = "KRI指标明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody T21KriInfoItem t21KriInfoItem)
    {
        return toAjax(t21KriInfoItemService.updateT21KriInfoItem(t21KriInfoItem));
    }

    /**
     * 删除KRI指标明细
     */
    @PreAuthorize("@ss.hasPermi('kri:item:remove')")
    @Log(title = "KRI指标明细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(t21KriInfoItemService.deleteT21KriInfoItemByIds(ids));
    }
}
