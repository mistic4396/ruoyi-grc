package domain.associate;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: qingshan
 */
public class AuthorAndBlog implements Serializable {
    private static final long serialVersionUID = 5575036101927170238L;
    Integer authorId; // 作者ID
    String authorName; // 作者名称
    List<BlogAndComment> blog; // 文章和评论列表

    @Override
    public String toString() {
        return "AuthorAndBlog{" +
                "authorId=" + authorId +
                ", authorName='" + authorName + '\'' +
                ", blog=" + blog +
                '}';
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public List<BlogAndComment> getBlog() {
        return blog;
    }

    public void setBlog(List<BlogAndComment> blog) {
        this.blog = blog;
    }
}
