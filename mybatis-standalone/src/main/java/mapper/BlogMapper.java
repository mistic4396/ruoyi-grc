package mapper;

import domain.Blog;
import domain.BlogExample;
import domain.associate.AuthorAndBlog;
import domain.associate.BlogAndAuthor;
import domain.associate.BlogAndComment;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * @Author: qingshan
 */
public interface BlogMapper {

    public int updateBlogList(List<Blog> list);
    public int updateByPrimaryKey(Blog blog);

    public Blog selectBlogById(Integer bid);
    public List<Blog> selectBlogListIf(Blog blog);

    public List<Blog> selectBlogByBean(Blog blog);

    public List<AuthorAndBlog> selectAuthorWithBlog();
    public BlogAndComment selectBlogWithCommentById(Integer bid);
    public BlogAndAuthor selectBlogWithAuthorQuery(Integer bid);
}
