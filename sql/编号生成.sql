/*
Navicat MySQL Data Transfer

Source Server         : 192.168.33.10_3306
Source Server Version : 50729
Source Host           : 192.168.33.10:3306
Source Database       : ruoyi-vue-activiti7

Target Server Type    : MYSQL
Target Server Version : 50729
File Encoding         : 65001

Date: 2021-01-17 09:48:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t01_seq
-- ----------------------------
DROP TABLE IF EXISTS `t01_seq`;
CREATE TABLE `t01_seq` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `code` varchar(64) NOT NULL COMMENT '编号',
  `name` varchar(64) DEFAULT NULL COMMENT '名称',
  `prefix` varchar(64) DEFAULT NULL COMMENT '前缀',
  `seq_index` bigint(20) DEFAULT '0' COMMENT '序号',
  `last_time` datetime NOT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='编号生成表';

-- ----------------------------
-- Records of t01_seq
-- ----------------------------
INSERT INTO `t01_seq` VALUES ('1', 'T001', '损失事件明细', 'LDC-itm', '2', '2021-01-15 00:00:00', '1');
INSERT INTO `t01_seq` VALUES ('2', 'T002', '损失事件', 'LDC', '2', '2021-01-15 00:00:00', null);
