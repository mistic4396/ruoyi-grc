package log;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;

public class LogTest {
    //private static final Log log = LogFactory.get();
    private static final Log log= LogFactory.get("我是一个自定义日志名");
    public static void main(String[] args) {
        log.debug("This is {} log", "xxx");
        log.info("This is {} log", "xxx");
        log.warn("This is {} log", "xxx");
    }
}
