package properties;

import cn.hutool.setting.dialect.Props;

public class PropsDemo {
    public static void main(String[] args) {
        Props props = new Props("test.properties");
        String user = props.getProperty("user");
        String driver = props.getStr("driver");
        System.out.println(user);
        System.out.println(driver);
    }
}
